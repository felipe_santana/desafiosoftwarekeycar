from google.cloud import pubsub_v1
from google.api_core import exceptions

project_id = 'desafio'


def list_topics():
    publisher = pubsub_v1.PublisherClient()
    project_path = publisher.project_path(project_id)

    for topic in publisher.list_topics(project_path):
        print(topic)


def create_topic(topic_name):
    try:
        publisher = pubsub_v1.PublisherClient()
        topic_path = publisher.topic_path(project_id, topic_name)

        topic = publisher.create_topic(topic_path)

        print('Topic created: {}'.format(topic))
    except exceptions.AlreadyExists:
        print('Topic already exists')


def delete_topic(topic_name):
    publisher = pubsub_v1.PublisherClient()
    topic_path = publisher.topic_path(project_id, topic_name)

    publisher.delete_topic(topic_path)

    print('Topic deleted: {}'.format(topic_path))


def create_subscription(topic_name, subscription_name):
    try:
        subscriber = pubsub_v1.SubscriberClient()
        topic_path = subscriber.topic_path(project_id, topic_name)
        subscription_path = subscriber.subscription_path(
            project_id, subscription_name)

        subscription = subscriber.create_subscription(
            subscription_path, topic_path)

        print('Subscription created: {}'.format(subscription))
    except exceptions.AlreadyExists:
        print('Subscription already exists')


def delete_subscription(subscription_name):
    subscriber = pubsub_v1.SubscriberClient()
    subscription_path = subscriber.subscription_path(
        project_id, subscription_name)

    subscriber.delete_subscription(subscription_path)

    print('Subscription deleted: {}'.format(subscription_path))


def list_subscriptions_in_topic(topic_name):
    publisher = pubsub_v1.PublisherClient()
    topic_path = publisher.topic_path(project_id, topic_name)

    for subscription in publisher.list_topic_subscriptions(topic_path):
        print(subscription)


def receive_messages(subscription_name):
    import time
    subscriber = pubsub_v1.SubscriberClient()
    subscription_path = subscriber.subscription_path(
        project_id, subscription_name)

    def callback(message):
        print('Received message: {}'.format(message))
        return message.ack()

    subscriber.subscribe(subscription_path, callback=callback)

    # print('Listening for messages on {}'.format(subscription_path))
    # while True:
    #     time.sleep(60)


receive_messages('testeAutoSub')
