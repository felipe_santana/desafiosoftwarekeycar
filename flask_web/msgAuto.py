from crontab import CronTab
from pubSub import create_subscription, create_topic, receive_messages

create_topic('testeAuto')
create_subscription('testeAuto', 'testeAutoSub')

cron = CronTab(user=True)
# job = cron.new(
#     command='python3 /home/felipe/Projects/desafio/flask_web/publish_message.py')
# job.minute.every(1)
# job.env['PUBSUB_EMULATOR_HOST'] = 'localhost:8085'
# job.env['PUBSUB_PROJECT_ID'] = 'desafio'
# job.enable()
for job in cron:
    cron.remove(job)
cron.write()

receive_messages('testeAutoSub')
