from peewee import *
from playhouse.postgres_ext import PostgresqlExtDatabase

pg_db = PostgresqlDatabase('msg', user='postgres', password='secret',
                           host='10.1.0.9', port=5432)

pg_db.connect()

pg_db.close()
