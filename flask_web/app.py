from pubSub import create_subscription, create_topic, receive_messages
from flask import Flask, jsonify, request, abort
import json
import datetime
from publish_message import publish_messages
app = Flask(__name__)


create_topic('testeAuto')
create_subscription('testeAuto', 'testeAutoSub')


@app.route('/mensagens', methods=['POST'])
def post_message():
    if not request.json:
        abort(400)

    result = publish_messages('testeAuto', request.json)

    return jsonify(result)


@app.route('/mensagens', methods=['GET'])
def get_messages():
    return jsonify(receive_messages('testeAutoSub'))


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
